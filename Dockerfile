# Dockerfile
FROM docker:27.5.1

RUN apk update \
    && apk add --no-cache \
       build-base \
       gcc \
       git \
       libc-dev \
       libffi-dev \
       make \
       openssl-dev \
       python3-dev \
       python3 \
       py-pip \
       util-linux \
       bash \
       curl \
       ca-certificates
